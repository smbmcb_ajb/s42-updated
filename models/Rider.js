const mongoose = require('mongoose')

const rider_schema = new mongoose.Schema({
	firstName: {
		type: String,
		required: [true, "First Name is required."]
	},
	lastName: {
		type: String,
		required: [true, "Last Name is required."]
	},
	email: {
		type: String,
		required: [true, "Email is required."]
	},
	password: {
		type: String,
		required: [true, "Password is required."]
	},
	
	verifier: {
		type: String
	},
	isVerified: {
		type: Boolean,
		default: false
	},
	isApproved: {
		type: Boolean,
		default: false
	},
	
	registeredOn: {
		type: Date,
		default: new Date()
	}
})

module.exports = mongoose.model('Rider', rider_schema)