const mongoose = require('mongoose')

const message_schema = new mongoose.Schema(
    
    [{
        fromUser: {
            type: String,
            required: [true, 'Sender is Required.']
        },
        toUser: 
            {
                type: String,
                required: [true, "Receiver is required."]
            },
        messages: 
            {
                type: String
            },
        
        sentOn: {
            type: Date,
            default: new Date()
        }
    }]
)

module.exports = mongoose.model('Message', message_schema)