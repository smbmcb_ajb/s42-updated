const Product = require('../models/Product')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')
const User = require('../models/User')
const Message = require('../models/Message')

module.exports.sendMessage = (data)=> {
    // console.log(data)
    let senderName, receiverName
    User.findById(data.userId).then((result)=> {
        senderName = result.firstName
    })

    User.findById(data.receiverId).then((result)=> {
        receiverName = result.firstName
    })

    let myMessage = [], receiverMessage = []
    let new_message = new Message(
        
            {
                fromUser: data.userId,
                toUser: data.receiverId,
                messages: data.message
            }
        
        
    )

    return new_message.save().then((newMessage, error)=> {
        if(error){
            return {
                message: "Something went wrong."
            }
        }
        // sender = newMessage.userId
        // receiver = newMessage.receiverId
        return newMessage
    })
    .then((result)=> {

        return Message.find().then((result)=> {
            // console.log(result)
            result.map((conversations)=> {
                if(data.userId == conversations.fromUser && data.receiverId == conversations.toUser){
                    receiverMessage.push(`${senderName}: ${conversations.messages}`)
                }

                if(data.userId == conversations.toUser && data.receiverId == conversations.fromUser){
                    myMessage.push(`${receiverName}: ${conversations.messages}`)
                }
            })
            return {
                to: myMessage,
                from: receiverMessage
            } 
               
        })
        })
    
}

module.exports.checkInbox = (data)=> {
    let myMessage = []
    // return Message.find({fromUser:data.userId},{},{$sort:{sentOn:1}}).then((result)=> {
    //     console.log(result)
    // })

    return Message.find().then((result)=> {
        result.map((conversations)=> {
            if(data.userId == conversations.toUser){
                    myMessage.push({
                        "SenderID": conversations.fromUser,
                        "Message": conversations.messages,
                        "Date Received" : conversations.sentOn
                    })
                }
        })
        return myMessage
    })

    // return Message.aggregate([
    //     {
    //         $match:{toUser:data.userId}
    //     },
        
    //     {$sort:{sentOn:-1}}
    // ]).then((result)=>{
    //     console.log(result)
    // })
}

module.exports.getMessages = async (data)=> {
    let myMessage = [] 
    let receiverMessage = []
    return Message.find().then((result)=> {
            // console.log(result)
            result.map((conversations)=> {
                if(data.userId == conversations.fromUser && data.receiverId == conversations.toUser){
                    myMessage.push({
                        "from": conversations.messages,
                        "sentOn": conversations.sentOn
                    })
                }

                if(data.userId == conversations.toUser && data.receiverId == conversations.fromUser){
                    myMessage.push({
                        "to": conversations.messages,
                        "sentOn": conversations.sentOn
                    })
                }
            })
            
           return myMessage
               
        })

}