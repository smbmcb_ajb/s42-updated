const User = require('../models/User')
// const Course = require('../models/Course')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')
// const { response } = require('express')
const Order = require('../models/Order')
const Product = require('../models/Product')
const { response } = require('express')
const jwt = require("jsonwebtoken")
const mail = require('../mail')
// const { findById } = require('../models/Product')


module.exports.register = (data) => {
    const secret = "E-commerce"
    var passPhrase = jwt.sign(data.email, secret, {})

    return User.find({email:data.email}, (success, error)=> {}).clone().catch((error)=>{
        return {message: "An error occured"}
    }).then((result)=> {
        if(result.length > 0){
            return {
                message: `duplicate`
            }
        }

        let encrypted_password = bcrypt.hashSync(data.password, 10)
    
        let new_user = new User({
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            password: encrypted_password,
            verifier: passPhrase
        })

        return new_user.save().then((created_user, error) => {
            if(error){
                return false
            }
            mail.sendEmail(new_user)
            return {

                message: `User ${new_user.firstName} Successfully registered. Please check your email to verify.`
            }
        })
        })
    
}

module.exports.login = (data) => {
    
    return User.findOne({email: data.email}).then((result) => {
        
        if(result == null){
            return {
                message: "User does not exist!"
            }
        }

        if(!result.isVerified){
            return {
                message: "Username is not yet verified. Please check your email for verification."
            }
        }
        
        const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if(is_password_correct) {
            let messages
            if(result.isAdmin){
                messages = `Hello there! Admin ${result.firstName}.`
            }
            else {
                messages = `Hello there! ${result.firstName}.`
            }

			return {
				accessToken:auth.createAccessToken(result),
                message: messages
			}
		}

		return {
			message: 'Password is incorrect!'
		}
    })
}

module.exports.getProfile = (user_data) => {
    // console.log(user_data)
    return User.findById(user_data.userId, {password:0}, (error, retrieved) => {}).clone().catch((error)=> {
        return false
    })
    .then(result => {
        if(result == null){
            return {
            message: "User does not exist!"
        }
        }
        return result
    })
}

module.exports.verifyEmail = (data) => {

    return User.findOneAndUpdate({
        $and: [
            {email: data.email},
            {verifier: data.verifier}
        ]
    }, {isVerified: true})
    .then((result)=> {
        // console.log(result)
        if(result == null || result.length === 0){
            return {
                status: "error",
                message: "Unknown verification access!"
            }
        }

        
            return {
                status: "success",
                message: "Email has been verified please proceed now to log-in page."
            }
        
    })
}

module.exports.getUserDetails = (data) => {
    return User.find({
        email: {$regex: data.search, $options: "$i"}       
    },
    {password:0},
     (error, retrieved) => {}).clone().catch((error) => { 
		return {
			message: "User does not exist!"
		}
	})
	.then((result) => {
        // console.log(result)
		if(result == null){
            return {
            message: "User does not exist!"
        }
        }
		return result
	})
}

module.exports.getAllUsers = () => {
    return User.find({},{password:0}).then((result) => {
        return result
    })
}

module.exports.changeUserTypetoAdmin = (user_id) => {
    // console.log(user_id)
    return User.findByIdAndUpdate(user_id, {isAdmin: true}, (error, result) => {}).clone().catch((error) => {
        return {
            message: "User does not exist!"
        }
    })
    .then((result) => {
        return User.findById({_id:result.id},{firstName:1, lastName:1, email:1,isAdmin:1})
    })
    .then((result) => {
        return {
            status: "success",
            message: `User ${result.firstName}'s user type has been changed to admin`,
            details: result
        }
    })
}

module.exports.changePassword = (user) => {
    let encrypted_oldPassword = bcrypt.hashSync(user.oldPassword, 10)
    let encrypted_newPassword = bcrypt.hashSync(user.newPassword, 10)

    return User.findById(user.userId, (error, result) => {}).clone().catch((error)=> {
        return {
            message: "Something went wrong."
        }
    })
    .then((result) => {
        const is_password_correct = bcrypt.compareSync(user.oldPassword, result.password)
        if(is_password_correct) {
            return User.findByIdAndUpdate(user.userId, {password: encrypted_newPassword, isVerified:true}, (error, result) => {}).clone().catch((error)=> {
                return {
                    message: "Something went wrong."
                }
            })
            .then((result) => {
                return {

                status: "success",
                message: "Password has been successfully updated"
                }
            })

        }else {
            return {
                message: "Access Denied!. Please check your password if it is correct."
            }
        }
})
}

module.exports.resetPassword = (user_id) => {
    new_password = Math.random().toString(36).slice(2, 11)
    let encrypted_password = bcrypt.hashSync(new_password, 10)
    return User.findByIdAndUpdate(user_id, {password: encrypted_password, isVerified: true}, (error, result) => {}).clone().catch((error) => {
        return {
            message: "User does not exist!"
        }
    })
    .then((result) => {
        return User.findById({_id:result.id},{firstName:1, lastName:1, email:1,isAdmin:1})
    })
    .then((result) => {

        return {
            status: "success",
            message: `User ${result.firstName}'s has been successfully reset to ${new_password}`,
            new_password: new_password
        }
    })
}

module.exports.assignUserToCsr = (user_id) => {
    return User.findByIdAndUpdate({_id:user_id}, {"isCustomerService": true}, (error, result)=> {}).clone().catch((error)=> {
        return{
            message: "Invalid User ID!"
        }
    })
    .then((result)=> {
        if(result._id){
            return {
                message: `${result.firstName} has been successfully assigned as CSR.`
            }
        }
        return result
    })
}



module.exports.viewCart = (user_id) => {
    return User.findById(user_id.userId,{cart:1}).then((result)=> {
        if(result == null){
            return {
                status: "error",
                message: "user does not exist."
            }
        }
        return result
    })
}

module.exports.changeProductQuantity = (data)=> {
    return Product.findById(data.productId, (resolve,error)=>{}).clone().catch((error)=> {
        return {
            message: "Unkown product Id"
        }
    })
    .then((result)=> {
        // console.log(result)
        return result
    }).then((result)=> {
        if(result == "" | result == null){
            // console.log(result)
            return {
                message: "Unkown product Id"
            }
        }
        if(result._id){
            return User.updateOne
            (
                {
                    $and: [
                        {
                            "cart.products": {"$elemMatch":{"productId": data.productId}},
                        },
                        {_id: data.userId}
                    ]
                },    
                {
                    "$set": {"cart.products.$.quantity":data.quantity, "cart.products.$.subTotal": data.quantity * result.price}
                }
            ).then((result)=> {
                if(result.modifiedCount){
                    return User.findById(data.userId).then((result)=> {
                        let total=0
                        result.cart.products.map((item)=> {
                            total += item.subTotal
                        })
                        
                        result.cart.totalAmount = total
                        return result.save().then((updated_total,error)=> {
                            if(error){
                                return{
                                    message: "An error occured!"
                                }
                            }
                            return {
                                status: "success",
                                message: "Product quantity has been successfully changed!"
                            }
                        })
                    })
                }
            if(result.matchedCount){
                return {
                    message: "Your desired quantity is the same as the quantity inside your cart. Change is not required"
                }
            }
            return {
                message: "Unable to update quantity. Product is not yet included in your shopping cart!"
        } 
    })
    }
        
        return result
    })
    
        
    
}

module.exports.removeItemFromTheCart = (data)=> {
    let message
    return User.findById(data.userId)
    .then((result)=> {
        for(let i = 0; i < result.cart.products.length; i++){
            if(data.productId == result.cart.products[i].productId){
                result.cart.totalAmount = result.cart.totalAmount - result.cart.products[i].subTotal
                message = `${result.cart.products[i].name} has been successfully removed from your shopping cart.`
                result.cart.products.splice(i, 1)
                
            }
        }
        return result.save().then((success, error)=> {
            if(error){
                return {
                    message: "Something went wrong."
                }
            }

            if(message){
                return {
                    status: "success",
                    message: message
                }
            }

            return {
                message: "Product does not exist in your shopping cart."
            }
        })
    })
}

module.exports.checkOut = async (data) =>{
    let isOderUpdated = await User.findById(data.userId).then((result)=> {
        if(result.cart.products.length == 0){
            return false
        }
        let new_order = new Order({
            userId: result._id,
            name: `${result.firstName} ${result.lastName}`,
            order: result.cart.products,
            totalAmount: result.cart.totalAmount
        })
        
        return new_order.save().then((success, error)=>{
            if(error){
                return false
            }

            // console.log(success)
            return true
        }) 
    })
    


    let isCartIsCleared = await User.findById(data.userId).then((result)=> {
        result.cart.products = []
        result.cart.totalAmount = 0
        return result.save().then((success, error)=>{
            if(error){
                return false
            }
            return true
        }) 

        
    })

    if(isOderUpdated && isCartIsCleared){
        return {
            status: "success",
            message: "Order has been placed. Please wait 2-3 business days for the delivery of your order/s"
        }
    }
    return {
        message: "There are no items in your shopping cart."
    }
}
