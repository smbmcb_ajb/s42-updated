const Order = require('../models/Order')
const Product = require('../models/Product')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')
const User = require('../models/User')




module.exports.getAllOrderHistory = (data) => {
    
    return Order.find({userId: data.userId},{userId:0}, (error, retrieved) => {
        return {
            message: "No orders yet."
        }
    }).clone().catch((error) => {
        
        return {
            message: "No orders yet."
        }
        
    })
    .then((result) => {
        
        if(result.length > 0){
            return result
        }
        
        return {
            message: "No orders yet."
        }
    })
}

module.exports.viewHistoryByStatus = (data) => {
    
    return Order.find({
        $and:[
            {userId: data.userId},
            {status:data.status}
        ]
        
    },{userId:0}, (error, retrieved) => {
        return {
            message: "No orders yet."
        }
    }).clone().catch((error) => {
        
        return {
            message: "No orders yet."
        }
        
    })
    .then((result) => {
        
        if(result.length > 0){
            return result
        }
        
        return {
            message: "No orders yet."
        }
    })
}

module.exports.viewOrders = (data) => {
    if(data.status == "all"){
        return Order.find()
        .then((result) => { 
            return result
        })
    }
    
    return Order.find({status:data.status}).then((result)=> {
        return result
    })
}

module.exports.updateOrderStatus = (data) => {
    // console.log(data)
    return Order.findById(data.orderId).then((result)=> {
        if(result.status == "completed"){
            return {message: "Unable to change status of completed orders!"}
        }

        return Order.findByIdAndUpdate(data.orderId, {status:data.status}, (result, error) => {}).clone().catch((error)=> {
            return {
                message: "Invalid Order ID!"
            }
        })
        .then((result)=> {
            return {
                status:'success',
                message: "Order status has been successfully updated."
            }
        })
    })
    
}

module.exports.assignRider = (data) => {
    return Order.findByIdAndUpdate(data.orderId, {status: "for-delivery", rider: data.rider}, (success, error)=>{}).clone().catch((error)=> {
         return {
                message: "Invalid Order ID!"
            }
    })
    .then((result)=> {
        
        if(result.id == null){
            return {
                message: "Invalid Order ID!"
            }
        }

        
            // console.log(result._id.length)
        return {
            message: `Order was successfully assigned to rider with id ${data.rider}.`
        }
    })
}