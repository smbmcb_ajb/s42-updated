const User = require('../models/User')
// const Course = require('../models/Course')
const bcrypt = require('bcrypt')
const auth = require('../auth.js')
// const { response } = require('express')
const Order = require('../models/Order')
const Product = require('../models/Product')
const { response } = require('express')
const jwt = require("jsonwebtoken")
// const { findById } = require('../models/Product')
const Rider = require('../models/Rider')
// const Product = require('../models/Product')
// const { response } = require('express')
// const { findById } = require('../models/Product')

module.exports.register = (data) => {
    const secret = "E-commerce"
    var passPhrase = jwt.sign(data.email, secret, {})

    return Rider.find({email:data.email}, (success, error)=> {}).clone().catch((error)=>{
        return {message: "An error occured"}
    }).then((result)=> {
        if(result.length > 0){
            return {
                message: `Duplicate email. ${data.email} is already in use.`
            }
        }

        let encrypted_password = bcrypt.hashSync(data.password, 10)
    
        let new_rider = new Rider({
            firstName: data.firstName,
            lastName: data.lastName,
            email: data.email,
            password: encrypted_password,
            verifier: passPhrase
        })

        return new_rider.save().then((created_user, error) => {
            if(error){
                return false
            }
            return {
                message: `${new_rider.firstName} has been successfully registered. Please check your email to verify`
            }
        })
        })
    
}

module.exports.login = (data) => {
    
    return Rider.findOne({email: data.email}).then((result) => {
        
        if(result == null){
            return {
                message: "User does not exist!"
            }
        }

        if(!result.isVerified){
            return {
                message: "Username is not yet verified. Please check your email for verification."
            }
        }
        
        if(!result.isApproved){
            return {
                message: "Not yet approve by the admin, please go to the nearest office."
            }
        }

        const is_password_correct = bcrypt.compareSync(data.password, result.password)

        if(is_password_correct) {
            let messages
            if(result.isAdmin){
                messages = `Hello there! Admin ${result.firstName}.`
            }
            else {
                messages = `Hello there! ${result.firstName}.`
            }

            return {
                accessToken:auth.createAccessTokenForRider(result),
                message: messages
            }
        }

        return {
            message: 'Password is incorrect!'
        }
    })
}

module.exports.verifyEmail = (data) => {

    return Rider.findOneAndUpdate({
        $and: [
            {email: data.email},
            {verifier: data.verifier}
        ]
    }, {isVerified: true})
    .then((result)=> {
        // console.log(result)
        if(result == null || result.length === 0){
            return {
                message: "Unknown verification access!"
            }
        }

        
            return {
                message: "Email has been verified please proceed no to log-in page."
            }
        
    })
}

module.exports.checkDelivery = (data)=> {
    // console.log(data)
    return Order.find({$and:[{status:"for-delivery"},{rider:data.userId}]})
    .then((result)=>{
        return result
    })
}

module.exports.approveARider = (data) => {
    
    return Rider.findByIdAndUpdate(data.riderId,{isApproved:true}, (success, error)=> {}).clone().catch((error)=> {
        return {
            message: "Invalid id!"
        }
    })
    .then((result)=>{
        if(!result.id){
            return result
        }
        return {
            message: `${result.firstName} has been successfully approved!`
        }
    })
}

module.exports.checkRiderApplication = (data) => {

    return Rider.find({isApproved:false},{password:0, verifier:0}).then((result)=> {

        return result
    })
}

module.exports.viewRiders = (data) => {

    return Rider.find({isApproved:true}, {password:0, verifier:0}).then((result)=> {

        return result
    })
}