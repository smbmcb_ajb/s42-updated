const express = require('express')
const router = express.Router()
const ProductController = require('../controllers/ProductController')
const auth = require('../auth')


router.post("/create", auth.verify, (request, response) => {

    
    const data = {
        product: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    if(request.body.name == "" || request.body.name == null || request.body.description == "" || request.body.description == null || request.body.price == "" || request.body.price == null)
    {
        return response.send({
            message: "All fields are required"
        })
    }

    ProductController.addProduct(data).then((result) => {
        // console.log(result)
        response.send(result)
    })
})

router.get("/all", (request, response) => {
    const data = request.headers.authorization
    
    ProductController.getAllProducts(data).then((result) => {
        response.send(result)
    })
})

router.post("/search", auth.verify, (request, response) => {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin,
        prodName: request.body.prodName,
        access: request.headers.authorization
    }
    // console.log(data.isAdmin)
    ProductController.searchProdName(data).then((result) => {
        response.send(result)
    })
})

router.get("/:id", (request, response) => {
    ProductController.getSpecificProduct(request.params.id).then((result) => {
        response.send(result)
    })
})


router.patch("/:id/update", auth.verify, (request, response) => {

    const data = {
        product: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }
    if(request.body.name == "" || request.body.name == null || request.body.description == "" || request.body.description == null || request.body.price == "" || request.body.price == null)
    {
        return response.send({
            message: "All fields are required"
        })
    }

    ProductController.updateProduct(request.params.id, request.body).then((result) => {
        response.send(result)
    })
})

router.patch("/:id/archive", auth.verify, (request, response) => {

    const data = {
        product: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    ProductController.archiveProduct(request.params.id).then((result) => {
        response.send(result)
    })
})

router.patch("/:id/unarchive", auth.verify, (request, response) => {

    const data = {
        product: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    ProductController.unArchiveProduct(request.params.id).then((result) => {
        response.send(result)
    })
})

router.post("/:id/addtocart/:quantity", auth.verify, (request, response) => {
    const data = {
        quantity: Number(request.params.quantity),
        productId: request.params.id,
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
// console.log(data)
    if(data.isAdmin){
        return response.send({message: "Admin doesn't have a cart!"})
    }

    ProductController.addToCart(data).then((result) => {
        response.send(result)
    })
})

router.post("/:id/test/:quantity", auth.verify, (request, response) => {
    const data = {
        quantity: Number(request.params.quantity),
        productId: request.params.id,
        userId: auth.decode(request.headers.authorization).id
    }

    ProductController.testChangeDataInsideArray(data).then((result) => {
        response.send(result)
    })
})


module.exports = router