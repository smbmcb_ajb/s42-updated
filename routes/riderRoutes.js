const express = require('express')
const router = express.Router()
const RiderController = require('../controllers/RiderController')
const auth = require('../auth')
const mail = require('../mail')

router.post("/register", mail.sendEmailToRider, (request, response) => {

    if(request.body.firstName == "" || request.body.firstName == null || request.body.lastName == "" || request.body.lastName == null || request.body.email == "" || request.body.email == null || request.body.password == "" || request.body.password == null){
        return response.send({
            message: "All fields are required"
        })
    }

    RiderController.register(request.body).then((result) => {
        response.send(result)
    })
})

router.get("/login", (request, response) => {
    RiderController.login(request.body).then((result) => {
        response.send(result)
    })
})

router.patch("/verify/:email/:passPhrase", (request, response) => {

    const data = {
        email: request.params.email,
        verifier: request.params.passPhrase
    }

    RiderController.verifyEmail(data).then((result)=> {
        response.send(result)
    })

    
})

router.get("/deliveries", auth.verify, (request, response)=> {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    // console.log(data.isAdmin)

    if(data.isAdmin){
        return response.send({message: "This page is for delivery riders only."})   
    }


    RiderController.checkDelivery(data).then((result)=> {
        response.send(result)
    })
})

router.patch("/admin/approve-a-rider/:id", auth.verify, (request, response) => {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin,
        riderId: request.params.id
    }

    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    RiderController.approveARider(data).then((result)=> {
        response.send(result)
    })
})

router.get("/admin/check-rider-application", auth.verify, (request, response)=> {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin,
        
    }

    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    RiderController.checkRiderApplication(data).then((result)=> {
        // console.log(result)
        response.send(result)
    })
})

router.get("/admin/view-riders", auth.verify, (request, response)=> {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin,
        
    }

    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    RiderController.viewRiders(data).then((result)=> {
        // console.log(result)
        response.send(result)
    })
})

module.exports = router