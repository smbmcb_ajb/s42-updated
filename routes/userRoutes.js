const express = require('express')
const router = express.Router()
const UserController = require('../controllers/UserController')
const auth = require('../auth')
const mail = require('../mail')

router.post("/register", (request, response) => {

    if(request.body.firstName == "" || request.body.firstName == null || request.body.lastName == "" || request.body.lastName == null || request.body.email == "" || request.body.email == null || request.body.password == "" || request.body.password == null){
        return response.send({
            message: "All fields are required"
        })
    }

    UserController.register(request.body).then((result) => {
        response.send(result)
    })
})

router.patch("/change-password", auth.verify, (request, response) => {
    const user_data = {
        userId: auth.decode(request.headers.authorization).id,
        newPassword : request.body.newPassword,
        oldPassword : request.body.oldPassword,
        confirmPassword : request.body.confirmPassword   
    }

    if(request.body.oldPassword == "" || request.body.oldPassword == null || request.body.newPassword == "" || request.body.newPassword == null || request.body.confirmPassword == "" || request.body.confirmPassword == null){
        return response.send({
            message: "All fields are required"
        })
    }

    if(!request.body.newPassword.match(request.body.confirmPassword)){
        return response.send({
            message: "Passwords dont match!"
        })
    }

    UserController.changePassword(user_data).then((result) => {
        response.send(result)
    })
})

router.post("/login", (request, response) => {
    UserController.login(request.body).then((result) => {
        response.send(result)
    })
})

router.get("/details", auth.verify, (request, response)=> {
    const user_data = {
        userId: auth.decode(request.headers.authorization).id
    }

    UserController.getProfile(user_data).then(result => {
        response.send(result)
    })
})

router.post("/search-user",auth.verify, (request, response) => {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin,
        search: request.body.search
    }
    // console.log(data)
    
    if(data.userId === request.params.id || data.isAdmin){
        UserController.getUserDetails(data).then((result) => {
            response.send(result)
        })
    } else {
        response.send({
            message: "You are not authorize to view other user's details"
        })
    }  
})

router.get("/", auth.verify, (request, response) => {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    
    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    UserController.getAllUsers().then((result) => {
        response.send(result)
    })
    
})

router.patch("/:id/change-usertype", auth.verify, (request, response) => {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    UserController.changeUserTypetoAdmin(request.params.id).then((result) => {
        response.send(result)
    })
})

router.patch("/:id/reset-password", auth.verify, (request, response) => {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    UserController.resetPassword(request.params.id).then((result) => {
        response.send(result)
    })
})

router.patch("/:id/assign-csr", auth.verify, (request, response)=> {
    const data = {
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(!data.isAdmin){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    UserController.assignUserToCsr(request.params.id).then((result)=> {
        return response.send(result)
    })
})



router.get("/cart", auth.verify, (request, response)=> {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(data.isAdmin){
        return response.send({message: "Admin doesn't have a cart!"})
    }


    UserController.viewCart(data).then((result)=> {
        return response.send(result)
    })

})

router.post("/cart/:id/change-quantity/:quantity", auth.verify, (request, response)=> {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        productId: request.params.id,
        quantity: Number(request.params.quantity),
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(data.isAdmin){
        return response.send({message: "Admin doesn't have a cart!"})
    }
    if(data.isAdmin == undefined){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    UserController.changeProductQuantity(data).then((result)=> {
        response.send(result)
    })
})

router.patch("/cart/:id/remove-item", auth.verify, (request, response)=> {
    const data = {
        userId: auth.decode(request.headers.authorization).id,
        productId: request.params.id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }

    if(data.isAdmin){
        return response.send({message: "Admin doesn't have a cart!"})
    }

    if(data.isAdmin == undefined){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    UserController.removeItemFromTheCart(data).then((result)=> {
        response.send(result)
    })
})

router.post("/check-out", auth.verify, (request, response) => {


    const data = {
		userId: auth.decode(request.headers.authorization).id,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

    if(data.isAdmin){
        return response.send({message: "Admin doesn't have a cart!"})
    }

    if(data.isAdmin == undefined){
        return response.send({
            message: "404 Page not found!",
            details: "directory does not exist!"
        })
    }

    UserController.checkOut(data).then((result) => {
        response.send(result)
    })
})


router.get("/verify/:email/:passPhrase", (request, response) => {

    const data = {
        email: request.params.email,
        verifier: request.params.passPhrase
    }
    // console.log(data)

    UserController.verifyEmail(data).then((result)=> {
        // console.log(result)
        response.send(result)
    })

    
})
module.exports = router